use std::{
    cmp::min,
    io::{BufRead, Stdin},
    mem::MaybeUninit,
};

// display dimensions
const SCREEN_HEIGHT: usize = 40;
const SCREEN_WIDTH: usize = 40;

const THETA_SPACING: f32 = 0.07;
const PHI_SPACING: f32 = 0.02;

const R1: f32 = 1.0;
const PI: f32 = std::f32::consts::PI;
const R2: f32 = 2.0;
const K2: f32 = 5.0;
// Calculate K1 based on screen size: the maximum x-distance occurs
// roughly at the edge of the torus, which is at x=R1+R2, z=0.  we
// want that to be displaced 3/8ths of the width of the screen, which
// is 3/4th of the way from the center to the side of the screen.
// screen_width*3/8 = K1*(R1+R2)/(K2+0)
// screen_width*K2*3/(8*(R1+R2)) = K1
const K1: f32 = SCREEN_WIDTH as f32 * K2 * 3.0 / (8.0 * (R1 + R2));

#[allow(clippy::many_single_char_names)]
fn render_frame(a: f32, b: f32) {
    // precompute sines and cosines of A and B
    let cos_a = a.cos();
    let sin_a = a.sin();
    let cos_b = b.cos();
    let sin_b = b.sin();

    let mut output = [[b' '; SCREEN_HEIGHT]; SCREEN_WIDTH];
    let mut zbuffer = [[0.0f32; SCREEN_HEIGHT]; SCREEN_WIDTH];

    // theta goes around the cross-sectional circle of a torus
    let mut theta = 0.0;
    while theta < 2.0 * PI {
        // precompute sines and cosines of theta
        let costheta = theta.cos();
        let sintheta = theta.sin();

        // phi goes around the center of revolution of a torus
        let mut phi = 0.0;
        while phi < 2.0 * PI {
            // precompute sines and cosines of phi
            let cosphi = phi.cos();
            let sinphi = phi.sin();

            // the x,y coordinate of the circle, before revolving (factored
            // out of the above equations)
            let circlex = R2 + R1 * costheta;
            let circley = R1 * sintheta;

            // final 3D (x,y,z) coordinate after rotations, directly from
            // our math above
            let x = circlex * (cos_b * cosphi + sin_a * sin_b * sinphi) - circley * cos_a * sin_b;
            let y = circlex * (sin_b * cosphi - sin_a * cos_b * sinphi) + circley * cos_a * cos_b;
            let z = K2 + cos_a * circlex * sinphi + circley * sin_a;
            let ooz = 1.0 / z; // "one over z"

            // x and y projection.  note that y is negated here, because y
            // goes up in 3D space but down on 2D displays.
            let xp = min(
                (SCREEN_WIDTH as f32 / 2.0 + K1 * ooz * x) as usize,
                SCREEN_WIDTH - 1,
            );
            let yp = min(
                (SCREEN_HEIGHT as f32 / 2.0 - K1 * ooz * y) as usize,
                SCREEN_HEIGHT - 1,
            );

            // calculate luminance.  ugly, but correct.
            let l = cosphi * costheta * sin_b - cos_a * costheta * sinphi - sin_a * sintheta
                + cos_b * (cos_a * sintheta - costheta * sin_a * sinphi);
            // L ranges from -sqrt(2) to +sqrt(2).  If it's < 0, the surface
            // is pointing away from us, so we won't bother trying to plot it.
            if l > 0.0 {
                // test against the z-buffer.  larger 1/z means the pixel is
                // closer to the viewer than what's already plotted.
                if ooz > zbuffer[xp][yp] {
                    zbuffer[xp][yp] = ooz;
                    let luminance_index = l * 8.0;
                    // luminance_index is now in the range 0..11 (8*sqrt(2)
                    // = 11.3) now we lookup the character corresponding to the
                    // luminance and plot it in our output:
                    output[xp][yp] = b".,-~:;=!*#$@"[luminance_index as usize];
                }
            }
            phi += PHI_SPACING;
        }
        theta += THETA_SPACING;
    }

    // now, dump output[] to the screen.
    // bring cursor to "home" location, in just about any currently-used
    // terminal emulation mode
    print!("\x1b[H");
    for output in &output {
        for c in output {
            print!("{}", *c as char)
        }
        println!("\r");
    }
}

struct HideCursor {
    backup: libc::termios,
}

impl HideCursor {
    /// Initializes raw mode.
    pub fn new() -> Self {
        let mut raw = unsafe {
            let mut raw = MaybeUninit::uninit();
            if libc::tcgetattr(libc::STDIN_FILENO, raw.as_mut_ptr()) != 0 {
                panic!("libc::tcgetattr failed");
            }
            raw.assume_init()
        };
        let backup = raw;
        raw.c_iflag &= !(libc::ICRNL | libc::IXON);
        raw.c_oflag &= !(libc::OPOST);
        raw.c_lflag &= !(libc::ECHO | libc::ICANON | libc::IEXTEN | libc::ISIG);
        raw.c_cc[libc::VMIN] = 0;
        raw.c_cc[libc::VTIME] = 1;

        if unsafe { libc::tcsetattr(libc::STDIN_FILENO, libc::TCSAFLUSH, &raw) } != 0 {
            panic!("libc::tcsetattr failed");
        }

        // clear screen
        print!("\x1b[2J");
        // hide the cursor
        print!("\x1b[?25l");
        // save the cursor
        print!("\x1b[s");

        Self { backup }
    }
}

impl Drop for HideCursor {
    fn drop(&mut self) {
        // restore the cursor
        print!("\x1b[u");
        // show the cursor
        print!("\x1b[?25h");
        if unsafe { libc::tcsetattr(libc::STDIN_FILENO, libc::TCSAFLUSH, &self.backup) } != 0 {
            eprintln!("libc::tcsetattr failed");
            std::process::abort();
        }
    }
}

struct HandleInput {
    stdin: Stdin,
    should_exit: bool,
}

impl HandleInput {
    fn new() -> Self {
        Self {
            stdin: std::io::stdin(),
            should_exit: false,
        }
    }

    fn read_stdin(&mut self) -> std::io::Result<()> {
        let mut lock = self.stdin.lock();
        let buf = lock.fill_buf()?;
        for &c in buf {
            if c == b'q' {
                self.should_exit = true;
            }
        }
        let len = buf.len();
        lock.consume(len);
        Ok(())
    }
}

fn main() -> std::io::Result<()> {
    const STEP_X: f32 = 0.002;
    const STEP_Y: f32 = 0.003;

    let _hide_cursor = HideCursor::new();
    std::thread::scope(|scope| {
        let handle = scope.spawn(|| {
            let mut handle_input = HandleInput::new();
            handle_input.read_stdin()?;
            loop {
                match handle_input.read_stdin() {
                    Ok(()) => {
                        if handle_input.should_exit {
                            return Ok(());
                        }
                    }
                    Err(err) => return Err(err),
                }
            }
        });
        let mut angle_x = 0.0;
        let mut angle_y = 0.0;
        while !handle.is_finished() {
            render_frame(angle_x, angle_y);
            angle_x += STEP_X;
            angle_y += STEP_Y;
        }
        handle.join().unwrap()
    })
}
